#include "loggerHeapSort.h"

double logNThreadsHeap(long size, char opt) {
        long* bigArr = (long*)malloc(size * sizeof(long));
        for (long i = 0; i < size; i++) {
            bigArr[i] = rand() % 1000000;
        }
        clock_t begin = clock();

        switch (opt)
        {
            case '1':
            heapSort(bigArr, size);
            break;

            case '2':
            heapSort2Threads(bigArr, size);
            break;

            case '3':
            heapSortNThreads(bigArr, size, 2);
            break;

            case '4':
            heapSortNThreads(bigArr, size, 4);
            break;
        }

        clock_t end = clock();
        double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
        free(bigArr);
        return time_spent;
}


int loggerHeapSort() {
    FILE *fp;
    if ((fp = fopen("heapSort.log", "w"))==NULL) {
        printf("Cannot open file.\n");
        exit(1);
    }
    long sizeFinish = 1000*10;
    long sizePlus = 500;
    fprintf(fp, "....................................................\n");
    fprintf(fp, "LOG FOR 1 THREAD\n");
    double time = logNThreadsHeap(1000, '1');
    fprintf(fp, "size: %lld, time: %f\n", 1000, time);
    fprintf(fp, "....................................................\n");

    /*fprintf(fp, "LOG FOR 2 THREADS USING SECTIONS\n");
    for (long size = 5000000; size<=sizeFinish; size+=sizePlus) {
        double time = logNThreadsHeap(size, '2');
        fprintf(fp, "size: %lld, time: %f\n", size, time);
    }
    fprintf(fp, "....................................................\n");

    fprintf(fp, "LOG FOR 2 THREADS USING FOR\n");
    for (long size = 5000000; size<=sizeFinish; size+=sizePlus) {
        double time = logNThreadsHeap(size, '3');
        fprintf(fp, "size: %lld, time: %f\n", size, time);
    }
    fprintf(fp, "....................................................\n");

    fprintf(fp, "LOG FOR 4 THREADS USING FOR\n");
    for (long size = 5000000; size<=sizeFinish; size+=sizePlus) {
        double time = logNThreadsHeap(size, '4');
        fprintf(fp, "size: %lld, time: %f\n", size, time);
    }
    fprintf(fp, "....................................................\n");
*/

    fclose(fp);
    return 0;
}


/*int smallLogger() {
    FILE *fp;
    if ((fp = fopen("mergeSortSmall.log", "w"))==NULL) {
        printf("Cannot open file.\n");
        exit(1);
    }
    long long sizeFinish = 1000*1000;
    long long sizePlus = 100000;
    fprintf(fp, "....................................................\n");
    fprintf(fp, "LOG FOR 1 THREAD\n");
    for (long long size = 100000; size<=sizeFinish; size+=sizePlus) {
        double time = logNThreads(size, '1');
        fprintf(fp, "size: %lld, time: %f\n", size, time);
    }
    fprintf(fp, "....................................................\n");

    fprintf(fp, "LOG FOR 2 THREADS USING SECTIONS\n");
    for (long long size = 100000; size<=sizeFinish; size+=sizePlus) {
        double time = logNThreads(size, '2');
        fprintf(fp, "size: %lld, time: %f\n", size, time);
    }
    fprintf(fp, "....................................................\n");

    fprintf(fp, "LOG FOR 2 THREADS USING FOR\n");
    for (long long size = 100000; size<=sizeFinish; size+=sizePlus) {
        double time = logNThreads(size, '3');
        fprintf(fp, "size: %lld, time: %f\n", size, time);
    }
    fprintf(fp, "....................................................\n");

    fprintf(fp, "LOG FOR 4 THREADS USING FOR\n");
    for (long long size = 100000; size<=sizeFinish; size+=sizePlus) {
        double time = logNThreads(size, '4');
        fprintf(fp, "size: %lld, time: %f\n", size, time);
    }
    fprintf(fp, "....................................................\n");


    fclose(fp);
    return 0;
}
*/
