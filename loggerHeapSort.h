#ifndef LOGGERHEAP_H
#define LOGGERHEAP_H
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include <time.h>
#include <string.h>

#include "heapSort.h"

int loggerHeapSort();
//int smallLogger();
double logNThreadsHeap(long size, char opt);




#endif
